package tutorial.javari.service;

import java.util.List;

import tutorial.javari.animal.Animal;

public interface AnimalService {

	Animal findById(long id);

	void saveAnimal(Animal animal);

	void deleteAnimalById(long id);

	List<Animal> findAllAnimals();

	boolean isAnimalIdExist(Animal animal);

}
