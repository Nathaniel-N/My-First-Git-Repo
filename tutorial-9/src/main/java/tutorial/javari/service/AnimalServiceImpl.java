package tutorial.javari.service;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.stereotype.Service;

import tutorial.javari.animal.Animal;
import tutorial.javari.animal.Condition;
import tutorial.javari.animal.Gender;

@Service("animalService")
public class AnimalServiceImpl implements AnimalService {

	private static List<Animal> animals;

	public AnimalServiceImpl() {
		FileInputStream fis = null;
		ArrayList<Animal> currentAnimals;

		try {

			String fileName = "lab/tutorial-9/src/main/resources/data.csv";

			fis = new FileInputStream(new File(fileName));
			CSVReader reader = new CSVReader(new InputStreamReader(fis));
			String[] nextLine = reader.readNext();

			animals = new ArrayList<>();

			if (nextLine != null) {
				while (nextLine != null) {
					Animal newAnimal = new Animal(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2],
							Condition.parseCondition(nextLine[3]), Double.parseDouble(nextLine[4]),
							Gender.parseGender(nextLine[5]), Double.parseDouble(nextLine[6]));
					animals.add(newAnimal);

					nextLine = reader.readNext();
				}
			}

		} catch (FileNotFoundException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("gada cok");
			System.out.println(System.getProperty("user.dir"));
			animals = null;
		} catch (IOException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			animals = null;
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException ex) {
				Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public List<Animal> findAllAnimals() {

		FileInputStream fis = null;
		ArrayList<Animal> currentAnimals;

		try {

			String fileName = "lab/tutorial-9/src/main/resources/data.csv";

			fis = new FileInputStream(new File(fileName));
			CSVReader reader = new CSVReader(new InputStreamReader(fis));
			String[] nextLine = reader.readNext();

			currentAnimals = new ArrayList<>();

			if (nextLine != null) {
				while (nextLine != null) {
					Animal newAnimal = new Animal(Integer.parseInt(nextLine[0]), nextLine[1], nextLine[2],
							Condition.parseCondition(nextLine[3]), Double.parseDouble(nextLine[4]),
							Gender.parseGender(nextLine[5]), Double.parseDouble(nextLine[6]));
					currentAnimals.add(newAnimal);
					nextLine = reader.readNext();
				}
			}

		} catch (FileNotFoundException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("gada cok");
			System.out.println(System.getProperty("user.dir"));
			currentAnimals = null;
		} catch (IOException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			currentAnimals = null;
		} finally {
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (IOException ex) {
				Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		if(currentAnimals == null) {
            return currentAnimals;
        }		

		if (animals.equals(currentAnimals)) {
			return null;
		} else {
			animals = currentAnimals;
			return animals;
		}
	}

	public Animal findById(long id) {
		for (Animal animal : animals) {
			if (animal.getId() == id) {
				return animal;
			}
		}
		return null;
	}

	public void saveAnimal(Animal animal) {
		animals.add(animal);

		FileWriter fw = null;

		try {

			String fileName = "lab/tutorial-9/src/main/resources/data.csv";

			fw = new FileWriter(fileName, true);
			CSVWriter writer = new CSVWriter(fw);
			String newAnimal = animal.getId() + "," + animal.getType() + "," + animal.getName() + ","
					+ animal.getCondition() + "," + animal.getLength() + "," + animal.getGender() + ","
					+ animal.getWeight();
			String[] newAnimalSplit = newAnimal.split(",");
			writer.writeNext(newAnimalSplit, false);

			writer.close();

		} catch (FileNotFoundException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("gada cok");
			System.out.println(System.getProperty("user.dir"));

		} catch (IOException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException ex) {
				Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	// public void updateAnimal(Animal animal) {
	// int index = animals.indexOf(animal);
	// animals.set(index, animal);
	// }

	public void deleteAnimalById(long id) {

		for (Iterator<Animal> iterator = animals.iterator(); iterator.hasNext();) {
			Animal animal = iterator.next();
			if (animal.getId() == id) {
				iterator.remove();
			}
		}

		FileInputStream fis = null;
		FileWriter fw = null;

		try {

			String fileName = "lab/tutorial-9/src/main/resources/data.csv";

			fis = new FileInputStream(new File(fileName));
			CSVReader reader = new CSVReader(new InputStreamReader(fis));

			String[] nextLine;
			List<String[]> allElements = new ArrayList<>();
			int idxCounter = -1;
			int rowNum = -1;

			while ((nextLine = reader.readNext()) != null) {
				allElements.add(nextLine);
				idxCounter++;
				if (Integer.parseInt(nextLine[0]) == id) {
					rowNum = idxCounter;
				}
			}

			allElements.remove(rowNum);

			fw = new FileWriter(fileName);
			CSVWriter writer = new CSVWriter(fw, ',', CSVWriter.NO_QUOTE_CHARACTER);
			writer.writeAll(allElements);

			writer.close();

		} catch (FileNotFoundException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			System.out.println("gada cok");
			System.out.println(System.getProperty("user.dir"));

		} catch (IOException ex) {
			Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);

		} finally {
			try {
				if (fw != null) {
					fw.close();
				}
			} catch (IOException ex) {
				Logger.getLogger(AnimalServiceImpl.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
	}

	public boolean isAnimalIdExist(Animal animal) {
		return findById(animal.getId()) != null;
	}

	public void deleteAllAnimals() {
		animals.clear();
	}

	// private static List<Animal> populateDummyAnimals(){
	// List<Animal> animals = new ArrayList<Animal>();
	// animals.add(new Animal((int)counter.incrementAndGet(),"Mouse", "Jerry",
	// Gender.MALE,15, 5, Condition.HEALTHY));
	// animals.add(new Animal((int)counter.incrementAndGet(),"Cat", "Tom",
	// Gender.MALE,65, 20, Condition.HEALTHY));
	// animals.add(new Animal((int)counter.incrementAndGet(),"Dog", "Bruno",
	// Gender.MALE,100, 50, Condition.HEALTHY));
	// return animals;
	// }

}
