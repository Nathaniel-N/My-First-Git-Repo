package id.ac.ui.cs.advprog.tutorial4.exercise1.factory;

import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.Cheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese.OreganoCheese;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.Clams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.clam.SteamedClams;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.Dough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.dough.HandTossedDough;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.MushroomSauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.sauce.Sauce;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.BlackOlives;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Eggplant;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Pineapple;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Spinach;
import id.ac.ui.cs.advprog.tutorial4.exercise1.factory.veggies.Veggies;

public class DepokPizzaIngredientFactory implements PizzaIngredientFactory {

    public Dough createDough() {
        return new HandTossedDough();
    }

    public Sauce createSauce() {
        return new MushroomSauce();
    }

    public Cheese createCheese() {
        return new OreganoCheese();
    }

    public Veggies[] createVeggies() {
        Veggies[] veggies = {new BlackOlives(), new Eggplant(), new Pineapple(), new Spinach()};
        return veggies;
    }

    public Clams createClam() {
        return new SteamedClams();
    }
}
