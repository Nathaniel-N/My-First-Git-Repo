package id.ac.ui.cs.advprog.tutorial4.exercise1.factory.cheese;

public class OreganoCheese implements Cheese {

    public String toString() {
        return "Oregano Cheese";
    }
}
