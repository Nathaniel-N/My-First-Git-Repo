package id.ac.ui.cs.advprog.tutorial3.composite.techexpert;

import id.ac.ui.cs.advprog.tutorial3.composite.Employees;
import jdk.nashorn.internal.runtime.regexp.joni.exception.ValueException;

public class NetworkExpert extends Employees {
    //TODO Implement
    public NetworkExpert(String name, double salary) {
        this.name = name;
        if (salary < 50000) {
            throw new IllegalArgumentException("Network Expert minimum salary: 50000");
        } else {
            this.salary = salary;
        }
        role = "Network Expert";
    }

    public double getSalary() {
        return salary;
    }

}
