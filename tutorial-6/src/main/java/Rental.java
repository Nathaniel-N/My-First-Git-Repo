import java.util.List;

class Rental {

    private Movie movie;
    private int daysRented;

    public Rental(Movie movie, int daysRented) {
        this.movie = movie;
        this.daysRented = daysRented;
    }

    public Movie getMovie() {
        return movie;
    }

    public int getDaysRented() {
        return daysRented;
    }

    public double getAmount() {
        double amount = 0;

        switch (this.getMovie().getPriceCode()) {
            case Movie.REGULAR:
                amount += 2;
                if (this.getDaysRented() > 2) {
                    amount += (this.getDaysRented() - 2) * 1.5;
                }
                break;
            case Movie.NEW_RELEASE:
                amount += this.getDaysRented() * 3;
                break;
            case Movie.CHILDREN:
                amount += 1.5;
                if (this.getDaysRented() > 3) {
                    amount += (this.getDaysRented() - 3) * 1.5;
                }
                break;
            default:
                break;
        }

        return amount;
    }

    public static int getFrequentRenterPoints(List<Rental> rentals) {
        int frequentRenterPoints = rentals.size();

        for (Rental rental : rentals) {
            // Add bonus for a two day new release rental
            if ((rental.getMovie().getPriceCode() == Movie.NEW_RELEASE)
                && rental.getDaysRented() > 1) {
                frequentRenterPoints++;
            }
        }

        return frequentRenterPoints;
    }

    public static double getTotalAmount(List<Rental> rentals) {
        double result = 0;
        for (Rental rental: rentals) {
            result += rental.getAmount();
        }

        return result;
    }


}