import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

class Customer {

    private String name;
    private List<Rental> rentals = new ArrayList<>();

    public Customer(String name) {
        this.name = name;
    }

    public void addRental(Rental arg) {
        rentals.add(arg);
    }

    public String getName() {
        return name;
    }

    public String statement() {
        int frequentRenterPoints = Rental.getFrequentRenterPoints(rentals);

        Iterator<Rental> iterator = rentals.iterator();
        String result = "Rental Record for " + getName() + "\n";

        while (iterator.hasNext()) {
            Rental each = iterator.next();
            
            // Show figures for this rental
            result += "\t" + each.getMovie().getTitle() 
                        + "\t" 
                        +  String.valueOf(each.getAmount()) 
                        + "\n";
        }

        // Add footer lines
        result += "Amount owed is " + String.valueOf(Rental.getTotalAmount(rentals)) + "\n";
        result += "You earned " + String.valueOf(frequentRenterPoints) + " frequent renter points";

        return result;
    }

    public String htmlStatement() {
        String result = "<h1>Rental Record for " + getName() + "</h1>";

        result += "<table>        <tr>        <th>title</th>        <th>amount</th>        </tr>";
        for (Rental rental: rentals) {
            result += "<tr>";
            result += "<td>" + rental.getMovie().getTitle() + "</td>";
            result += "<td>" + String.valueOf(rental.getAmount()) + "</td>";
            result += "</tr>";
        }
        result += "</table>";

        // Add footer lines
        result += "<h3>Amount owed is " + String.valueOf(Rental.getTotalAmount(rentals)) + "</h3>";
        result += "<h3>You earned " + String.valueOf(Rental.getFrequentRenterPoints(rentals));
        result += " frequent renter points</h3>";

        return result;
    }

}